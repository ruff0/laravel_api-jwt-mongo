## Installation

git clone https://bitbucket.org/ruff0/laravel_api-jwt-mongo-for-zipdev.git
cd laravel_api-jwt-mongo-for-zipdev
composer install -vvv

seed the database to get an user.

./artisan migrate --seed

check your .env settings and then :

./artisan serve

## Usage
post this json to localhost:8000/api/login :

{
  "email": "just@test.mail",
  "password": "password"
}

you will get an access_token to use as bearer.
now you can use the crud.
type :
./artisan route:list

to see routes.

json example:

{
  "firstName": "john",
  "surName": "wallace",
  "phones":
  [
    {"number": "1234567890"},
    {"number": "0987654321"}
  ],
  "emails":
  [
    {"address": "jonh@fake.mail"},
    {"address": "jonh_w@fake.mail"}
  ]
}
