<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Contact extends Eloquent
{
    protected $collection = 'contact';

    protected $fillable = ['firstName', 'surName', 'picture', 'phones', 'emails'];

    public function phones()
    {
      return $this->hasMany('App\Phone');
    }

    public function emails()
    {
      return $this->hasMany('App\Email');
    }
}
