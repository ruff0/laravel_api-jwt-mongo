<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Phone extends Eloquent
{
  protected $collection = 'phone';

  protected $fillable = ['contact_id', 'number'];

  public $rules = [
      'contact_id' => 'required',
      'number' => 'required',
  ];
}
