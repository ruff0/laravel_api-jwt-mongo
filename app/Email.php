<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Email extends Eloquent
{
  protected $collection = 'email';

  protected $fillable = ['contact_id', 'address'];

  public $rules = [
      'contact_id' => 'required',
      'address' => 'required',
  ];
}
