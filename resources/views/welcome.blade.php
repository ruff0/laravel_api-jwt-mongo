<div class="container">
<div >
  <h2>Install and run</h1>


  <p>git clone https://bitbucket.org/ruff0/laravel_api-jwt-mongo-for-zipdev.git</p>
  <p>cd laravel_api-jwt-mongo-for-zipdev</p>
  <p>composer install && composer update</p>
  <p>./artisan migrate --seed</p>
  <p>./artisan runserver</p>


  <h2>Usage</h2>
  <p>
  POST this json in http://158.69.208.77/api/login/ to login:</br>
</br>
  {</br>
    "email": "just@test.mail",</br>
    "password": "password"</br>
  }</br>
  </br>
  you will get an access_token to use as bearer.</br>
</br>
  this is the route for the api, you can use:</br>
  GET    &nbsp;&nbsp;&nbsp;http://158.69.208.77/api/contact/ &nbsp;&nbsp;&nbsp;      to get contact list.</br>
  POST   &nbsp;&nbsp;&nbsp;http://158.69.208.77/api/contact/ &nbsp;&nbsp;&nbsp;      to create a new contact.</br>
  GET    &nbsp;&nbsp;&nbsp;http://158.69.208.77/api/contact/id/ &nbsp;&nbsp;&nbsp; to retrieve contact.</br>
  PUT    &nbsp;&nbsp;&nbsp;http://158.69.208.77/api/contact/id/ &nbsp;&nbsp;&nbsp; to update a contact.</br>
  DELETE &nbsp;&nbsp;&nbsp;http://158.69.208.77/api/contact/id/ &nbsp;&nbsp;&nbsp; to delete a contact.</br>
    </br>
  json example:</br>
  </br>
  {</br>
    "firstName": "john",</br>
    "surName": "wallace",</br>
    "phones":</br>
    [</br>
      {"number": "1234567890"},</br>
      {"number": "0987654321"}</br>
    ],</br>
    "emails":</br>
    [</br>
      {"address": "jonh@fake.mail"},</br>
      {"address": "jonh_w@fake.mail"}</br>
    ]</br>
  }</br>
</p>
